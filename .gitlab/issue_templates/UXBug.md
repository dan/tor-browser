<!--
* Use this issue template for reporting a new UX bug.
-->

### Summary
**Summarize the bug encountered concisely.**


### Steps to reproduce:
**How one can reproduce the issue - this is very important.**

1. Step 1
2. Step 2
3. ...

### What is the current bug behavior?
**What actually happens.**


### What is the expected behavior?
**What you want to see instead**



## Relevant logs and/or screenshots
**Do you have screenshots? Attach them to this ticket please.**

/label ~tor-ux ~needs-investigation ~bug
/assign @nah
